let win = require('electron').remote.getCurrentWindow();

document.addEventListener("keyup", onKeyPress);

function onKeyPress() {
    if (win.isMenuBarVisible()) {
        win.setMenuBarVisibility(false);
    } else {
        win.setMenuBarVisibility(true);
    }
}