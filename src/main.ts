import { app, BrowserWindow} from 'electron';
import * as path from "path";


let win: BrowserWindow = null;

app.on("ready", () => {

    win = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            nodeIntegration: true
        }
    });
    // and load the index.html of the app.
    win.loadFile(path.join(__dirname, "../index.html"));

});